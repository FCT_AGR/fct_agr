from builtins import print
from django import forms
from .models import Guia, Proyecto, Cadena, Plataforma
from django.utils.translation import gettext_lazy as _
import html



class GuiaForm(forms.ModelForm):
    proyecto_list = list()
    for proyec in Proyecto.objects.all():
        lista = list()
        lista.append(html.unescape(proyec.nombre))
        lista.append(html.unescape(proyec.nombre))
        proyecto_list.append(lista)

    cadena_list = list()
    for cad in Cadena.objects.all():
        lista = list()
        lista.append(html.unescape(str(cad)))
        lista.append(html.unescape(cad.nombre))
        cadena_list.append(lista)

    proyecto = forms.ChoiceField(label="Proxecto",
                                 choices=proyecto_list,
                                 widget=forms.Select(attrs={'class':"form-control"}))
    cadena = forms.ChoiceField(label="Cadea",
                               choices=cadena_list,
                               widget=forms.Select(attrs={'class': "form-control"}))
    plataforma = forms.ModelChoiceField(label='Plataforma',
                                        queryset=Plataforma.objects.all(),
                                        widget=forms.Select(attrs={'class': "form-control"}))
    version = forms.CharField(label='Versión',
                              widget=forms.TextInput(attrs={'class': "form-control"}))
    txt = forms.CharField(label='Texto da guía',
                          widget=forms.Textarea(attrs={'class': "form-control"}))

    def clean(self):
        cleaned_data = super(GuiaForm, self).clean()
        proyecto_nombre = cleaned_data.get('proyecto')
        cadena_nombre = cleaned_data.get('cadena')
        cleaned_data['proyecto'] = Proyecto.objects.get(nombre=proyecto_nombre)
        cleaned_data['cadena'] = Cadena.objects.get(nombre=" ".join(cadena_nombre.split(' ')[2:]), proyecto=cleaned_data['proyecto'].id)

    class Meta:
        model = Guia
        fields = ('proyecto', 'cadena', 'plataforma', 'version', 'txt')
