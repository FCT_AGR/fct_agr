from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Proyecto)
admin.site.register(models.Cadena)
admin.site.register(models.Plataforma)

admin.site.register(models.ExpresionRegular)

admin.site.register(models.DiccionarioDimensiones)
admin.site.register(models.Dimension)

admin.site.register(models.DiccionarioVariable)
admin.site.register(models.Variable)

admin.site.register(models.Guia)
admin.site.register(models.ErrorGuia)

admin.site.register(models.Json)
admin.site.register(models.Seccion)
admin.site.register(models.Subseccion)
admin.site.register(models.Vista)
admin.site.register(models.DimensionesPageview)
admin.site.register(models.Evento)
admin.site.register(models.DimensionesEvento)

#admin.site.register(models.PlantillaXML)
#admin.site.register(models.PlantillaCSV)

admin.site.register(models.Test)