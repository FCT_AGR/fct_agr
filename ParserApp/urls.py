from django.conf.urls import url
from . import views

app_name = 'parserapp'

urlpatterns = [
    url(r'^$', views.home_page, name='home_app'),
    url(r'^guias/$', views.GuiasList.as_view(), name='guias'),
    url(r'^guias/(?P<pk>\d+)/$', views.GuiasDetail.as_view(), name='guia_detail'),
    url(r'^guias/EditarGuia/(?P<pk>\d+)/$', views.GuiaUpdateView.as_view(), name='guia_update'),
    url(r'^guias/NuevaGuia/$', views.CreateGuiaView.as_view(), name='nueva_guia'),
    url(r'^guias/GuardarXml/(?P<pk>\d+)/$', views.guardar_xml, name='guardar_xml'),
    url(r'^guias/GuardarCsv/(?P<pk>\d+)/$', views.guardar_csv, name='guardar_csv'),
    url(r'^diccionariosdimesiones/lista/$', views.DiccionariosDimensionesList.as_view(), name='diccionarios_dimensiones'),
    url(r'^diccionariosdimensiones/(?P<pk>\d+)/$', views.DiccionarioDimensionesDetail.as_view(), name='diccionarios_dimensiones_detail'),
    url(r'^diccionariosvariable/lista/$', views.DiccionariosVariableList.as_view(), name='diccionarios_variable'),
    url(r'^diccionariosvariable/(?P<pk>\d+)/$', views.DiccionarioVariableDetail.as_view(), name='diccionarios_variable_detail'),
    url(r'^ExpresionRegular/$', views.ExpresionRegular.as_view(), name='expresionregular'),
    url(r'test/all/$', views.test, name='test_all'),
]
