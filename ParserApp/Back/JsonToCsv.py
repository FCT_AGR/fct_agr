# pip install unicodecsv
import io
import traceback

import unicodecsv as csv

from ParserApp import models
from ParserApp.Back.utils_xml import SerializarJson


class ArchivarCsv:
    def __init__(self, guia):
        self.guia = guia
        self.primera_linea = ["VISTA", "CLASE FUNCIONAL", "CATEGORÍA", "ACCIÓN", "ETIQUETA", "DIMENSIONES"]
        self.seccion_error = 'ToCsv'
        self.json_entrada = SerializarJson.getJson(self.guia)
        self.csv_salida = io.BytesIO()
        self.csv_writer = csv.writer(self.csv_salida, delimiter=";", lineterminator='\n',)
        self.archivarErrores = False

    def convertir_a_csv(self, archivarErrores=False):
        self.csv_writer.writerow(self.primera_linea)
        self.archivarErrores = archivarErrores

        try:
            for seccion in self.json_entrada:
                try:
                    self.convertir_seccion(seccion)
                except Exception:
                    print('---------------------------')
                    traceback.print_exc()
                    print('Error en subsección\nÚltima vista Ok: ' + subseccion['Vistas'])

                    if self.archivarErrores:
                        models.ErrorGuia.objects.create(
                            guia=self.guia,
                            seccion=self.seccion_error,
                            error='Error en subsección después de: '
                        )
                    print('---------------------------\n')
        except Exception:
            print('---------------------------')
            traceback.print_exc()
            print('Error en Sección\n')

            if self.archivarErrores:
                models.ErrorGuia.objects.create(
                    guia=self.guia,
                    seccion=self.seccion_error,
                    error='Error en Sección después de: ' + subseccion['Vistas']
                )
            print('---------------------------\n')


        return self.csv_salida.getvalue()
        # csv_obj = models.PlantillaCSV(guia=self.guia)
        # csv_obj.datos_csv = self.csv_salida.getvalue()
        # csv_obj.save()

    def archivar_dimensiones_pageview(self, vista):
        listado_dimensiones_pageview = ""
        for dimensionesPageview in vista["Eventos y dimensiones"][1]:
            if listado_dimensiones_pageview:
                listado_dimensiones_pageview += ", " + dimensionesPageview
            else:
                listado_dimensiones_pageview += dimensionesPageview
        self.csv_writer.writerow(["", "", "", "", "", listado_dimensiones_pageview])

    def archivar_eventos(self, evento):
        listado_dimensiones_evento = ""
        for dimensionEvento in evento['Dimens.']:
            if listado_dimensiones_evento:
                listado_dimensiones_evento += ", " + dimensionEvento
            else:
                listado_dimensiones_evento += dimensionEvento
        self.csv_writer.writerow(
            ["", evento['C.F.'], evento['Categoría'], evento['Acción'], evento['Etiqueta'], listado_dimensiones_evento])

    def convertir_seccion(self, seccion):
        for subseccion in seccion["subseccion"]:
            if 'subseccion' in subseccion:
                self.convertir_seccion(subseccion)
            try:
                if 'Vistas' in subseccion:
                    for vista in subseccion['Vistas']:
                        self.csv_writer.writerow([(vista["Vista"] + "\n" + vista["Pagename"])])
                        try:
                            self.archivar_dimensiones_pageview(vista)
                        except Exception:
                            traceback.print_exc()
                        try:
                            for evento in vista["Eventos y dimensiones"][0]:
                                self.archivar_eventos(evento)
                        except Exception:
                            print('---------------------------')
                            traceback.print_exc()
                            print('Error en evento\nÚltimo evento Ok: ' + str(evento))

                            if self.archivarErrores:
                                models.ErrorGuia.objects.get_or_create(
                                    guia=self.guia,
                                    seccion=self.seccion_error,
                                    error='Error en evento. Último evento Ok: ' + str(evento) + ' de la vista ' + vista['Vista']
                                )
                            print('---------------------------\n')
            except Exception:
                print('---------------------------')
                traceback.print_exc()
                print('Error en vista\nÚltima vista Ok: ' + vista['Vista'])

                if self.archivarErrores:
                    models.ErrorGuia.objects.get_or_create(
                        guia=self.guia,
                        seccion=self.seccion_error,
                        error=r'Error en vista después de: ' + vista['Vista']
                    )
                print('---------------------------\n')