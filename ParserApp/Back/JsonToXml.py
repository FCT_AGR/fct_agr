import traceback
import xml.etree.cElementTree as ET

from ParserApp import models
from ParserApp.Back.utils_xml import DictDimensiones
from ParserApp.Back.utils_xml import CargarPropiedades, SerializarJson


class ConvertirAXml:
    def __init__(self, guia):
        self.guia = guia
        self.validator_name = str(self.guia)
        self.dispositivo = self.guia.plataforma
        self.diccionarios = DictDimensiones.get_diccionarios(self.guia.cadena.nombre, self.dispositivo)
        self.seccion_error = 'ToXml'
        self.version_comentario = "Creado con JsonToXml v.0.2"
        self.cargardor = CargarPropiedades.CargadorPropiedades(self.guia, self.dispositivo, self.diccionarios)
        self.archivarErrores = False

        self.xml_root = ET.Element('validator-template')
        self.xml_root.append(ET.Comment(self.version_comentario))

        self.xml_validator_name = ET.SubElement(self.xml_root, 'name')
        self.xml_validator_name = self.validator_name

    def convertir_a_xml(self, archivarErrores=False):
        json_entrada = SerializarJson.getJson(self.guia)
        self.archivarErrores = archivarErrores

        for seccion in json_entrada:
            try:
                self.serializar_subseccion(seccion)
            except Exception:
                traceback.print_exc()
                if self.archivarErrores:
                    models.ErrorGuia.objects.create(
                        guia=self.guia,
                        seccion=self.seccion_error,
                        error="Error en Subsección: " + str(seccion)
                )
                print("Error en Subsección: " + str(seccion) + "\n---------------------------------------------\n\n")

        # xml_formateado = ET.tostring(self.xml_root, encoding='utf8', method='xml')
        xml_formateado = ET.tostring(self.xml_root, encoding='iso-8859-1', method='xml')
        return xml_formateado

    def serializar_subseccion(self, seccion):
        for subseccion in seccion['subseccion']:
            if 'subseccion' in subseccion:
                self.serializar_subseccion(subseccion)
            try:
                if 'Vistas' in subseccion:
                    for vista in subseccion["Vistas"]:
                        self.serializar_vista(vista)
            except Exception:
                traceback.print_exc()
                if self.archivarErrores:
                    models.ErrorGuia.objects.create(
                        guia=self.guia,
                        seccion=self.seccion_error,
                        error="Error en Vistas después de " + vista['Vista']
                    )
                print("Error en Vistas:" + self.validator_name +
                      "\n\nÚltima vista ok: " + vista['Vista'] + "\n---------------------------------------------\n\n")

    def serializar_vista(self, vista):
        # Validador con cd68: al nombre de la vista o del evento se le añade +
        nombre_vista = vista['Vista']
        self.xml_hitValidators = ET.SubElement(self.xml_root, 'hitValidators')
        ET.SubElement(self.xml_hitValidators, 'view-name').text = nombre_vista + '+'
        ET.SubElement(self.xml_hitValidators, 'type').text = 'Tipo de Hit'
        self.cargardor.propiedades_pageview(self.xml_hitValidators, vista)
        for evento in vista['Eventos y dimensiones'][0]:
            xml_hitValidators = ET.SubElement(self.xml_root, 'hitValidators')
            ET.SubElement(xml_hitValidators, 'view-name').text = nombre_vista + " - " + evento['Acción'] + '+'
            ET.SubElement(xml_hitValidators, 'type').text = 'Tipo de Hit'
            self.cargardor.propiedades_evento(xml_hitValidators, evento, vista)

        # Validador sin cd68
        nombre_vista = vista['Vista']
        self.xml_hitValidators = ET.SubElement(self.xml_root, 'hitValidators')
        ET.SubElement(self.xml_hitValidators, 'view-name').text = nombre_vista
        ET.SubElement(self.xml_hitValidators, 'type').text = 'Tipo de Hit'
        self.cargardor.propiedades_pageview(self.xml_hitValidators, vista, False)
        for evento in vista['Eventos y dimensiones'][0]:
            xml_hitValidators = ET.SubElement(self.xml_root, 'hitValidators')
            ET.SubElement(xml_hitValidators, 'view-name').text = nombre_vista + " - " + evento['Acción']
            ET.SubElement(xml_hitValidators, 'type').text = 'Tipo de Hit'
            self.cargardor.propiedades_evento(xml_hitValidators, evento, vista, False)