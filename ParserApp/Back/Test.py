from difflib import unified_diff
import sys
from ..models import Guia, Test
from .ToJson import get_json


def print_diff(original, nuevo):
    for line in unified_diff(original, nuevo):
        print(line)


def make_all_tests():
    test_obj = Test.objects.all()

    for test in test_obj:
        print('\n----------------------\n')
        print('\nComparando la guía ' + str(test.guia))

        json_original = test.json.replace("'", '"')
        json_nuevo = get_json(test.guia.txt)
        print('Comparando json-------------')
        print_diff(json_original, json_nuevo)

        # xml_original = test.xml
        # xml_nuevo = test.guia.xml.datos_xml
        # print('Comparando xml-------------')
        # print_diff(xml_original, xml_nuevo)

        csv_original = test.csv
        csv_nuevo = test.guia.csv.datos_csv.replace('\n', '\r\n')
        print('Comparando csv-------------')
        print_diff(csv_original, csv_nuevo)
