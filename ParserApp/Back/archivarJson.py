import traceback
from django.db import transaction
from ParserApp import models


class Archivar:
    def __init__(self, json, guia):
        self.json = json
        self.guia = guia
        self.seccion_error = 'ToJson'

    def archivar(self):
        json_obj = models.Json.objects.create(
            guia=self.guia,
            txt=self.json
        )
        try:
            with transaction.atomic():
                for seccion in self.json:
                    self.archivar_seccion(seccion, json_obj)
        except:
            print('---------------------------')
            traceback.print_exc()
            print('Error en sección')
            self.archivar_error('Error procesando el json: ')
            print('---------------------------\n')

        transaction.commit()

    def archivar_seccion(self, seccion, json_obj):
        if 'Nombre' not in seccion:
            seccion['Nombre'] = '****ERROR Sección'
            self.archivar_error('Error en el nombre de la sección: ****ERROR Sección')

        if 'Índice' not in seccion:
            seccion['Índice'] = '****ERROR Sección'
            self.archivar_error('Error en el Índice de la sección: ****ERROR Sección')

        seccion_obj = models.Seccion.objects.create(
            json=json_obj,
            nombre=seccion['Nombre'],
            indice=seccion['Índice']
        )

        # if 'subseccion' not in seccion: continue
        try:
            for subseccion in seccion['subseccion']:
                self.archivar_subseccion(subseccion, seccion_obj)
        except:
            print('---------------------------')
            traceback.print_exc()
            self.archivar_error('Error en subsección:: ' + str(seccion['subseccion']))
            print('---------------------------\n')

    def archivar_subseccion(self, subseccion, seccion_obj, hijo=False):
        if 'Nombre' not in subseccion:
            if type(subseccion) is not dict:
                subseccion = {'Nombre': '****ERROR Subsección\n ' + str(subseccion)}
            else:
                subseccion['Nombre'] = '****ERROR Subsección'
        if 'Índice' not in subseccion:
            subseccion['Índice'] = '****ERROR Subsección'

        if hijo:
            subseccion_obj = models.Subseccion.objects.create(
                subseccion_padre=seccion_obj,
                nombre=subseccion['Nombre'],
                indice=subseccion['Índice']
            )
        else:
            subseccion_obj = models.Subseccion.objects.create(
                seccion=seccion_obj,
                nombre=subseccion['Nombre'],
                indice=subseccion['Índice']
            )

        try:
            if 'subseccion' in subseccion:
                for subseccion_hijo in subseccion:
                    self.archivar_subseccion(subseccion_hijo, subseccion, hijo=True)

            elif 'Vistas' in subseccion:
                for vista in subseccion['Vistas']:
                    self.archivar_vista(vista, subseccion_obj)
            else:
                self.archivar_error('Subsección vacía: ' + str(subseccion['Nombre']))
        except:
            print('---------------------------')
            traceback.print_exc()
            print('Error en vista\nÚltimo evento Ok: ' + str(evento))
            self.archivar_error('Error en vista después de la vista: ' + str(vista['Vista']))
            print('---------------------------\n')

    def archivar_vista(self, vista, subseccion_obj):
        if 'Pagename' not in vista: vista['Pagename'] = ''
        if 'Page title' not in vista: vista['Page title'] = ''
        if 'Page type' not in vista: vista['Page type'] = ''
        if 'Sección' not in vista: vista['Sección'] = ''
        if 'Comentarios' not in vista: vista['Comentarios'] = ''
        vista_obj = models.Vista.objects.create(
            subseccion=subseccion_obj,
            vista=vista['Vista'],
            pagename=vista['Pagename'],
            pagetitle=vista['Page title'],
            pagetype=vista['Page type'],
            seccion=vista['Sección'],
            comentarios=vista['Comentarios'],
        )
        try:
            for dimensionPv in vista['Eventos y dimensiones'][1]:
                dimension_pv_obj = models.DimensionesPageview.objects.create(
                    vista=vista_obj,
                    dimension=dimensionPv
                )
            for evento in vista['Eventos y dimensiones'][0]:
                self.archivar_evento(evento, vista_obj)
        except:
            print('---------------------------')
            traceback.print_exc()
            print('Error en Eventos o dimensión pageview\nÚltimo evento Ok: ' + str(evento))
            self.archivar_error('Error en Eventos o dimensiónes del pageview: ' + str(vista['Vista']))
            print('---------------------------\n')

    def archivar_evento(self, evento, vista_obj):
        evento_obj = models.Evento.objects.create(
            vista=vista_obj,
            accion=evento['Acción'],
            clase_funcional=evento['C.F.'],
            categoria=evento['Categoría'],
            etiqueta=evento['Etiqueta'],
            lanzamiento=evento['Lanzamiento']
        )
        try:
            if 'Dimens.' in evento:
                for dimension in evento['Dimens.']:
                    dimension_evento_obj = models.DimensionesEvento.objects.create(
                        evento=evento_obj,
                        dimension=dimension
                    )
        except:
            print('---------------------------')
            traceback.print_exc()
            print('Error en dimensión evento\nÚltimo evento Ok: ' + str(evento))
            self.archivar_error('Error en las dimensines del evento: ' + str(evento))
            print('---------------------------\n')

    def archivar_error(self, mensaje):
        models.ErrorGuia.objects.create(
            guia=self.guia,
            seccion=self.seccion_error,
            error=mensaje
        )