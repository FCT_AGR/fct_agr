import io, json

import cProfile
from ParserApp.Back.utils_json import ParseadorGuia as prs_g
from ParserApp.Back import archivarJson


# ESTRUCTURA:
# [
#     {
#         "Índice": "",
#         "Nombre": "",
#         "subseccion": [
#             {
#                 "Nombre": "",
#                 "Vistas": [
#                     {
#                         "Eventos y dimensiones": [
#                             [
#                                 {
#                                     "Acción": "",
#                                     "C.F.": "",
#                                     "Categoría": "",
#                                     "Dimens.": [
#                                         ""
#                                     ],
#                                     "Etiqueta": "",
#                                     "Lanzamiento": ""
#                                 },
#                             ],
#                             []
#                         ],
#                         "Page title": "",
#                         "Page type": "",
#                         "Pagename": "",
#                         "Sección": "",
#                         "Vista": "",
#                         "Comentarios": "",
#                     }
#                 ],
#                 "Índice": ""
#             },
# 	}
# ]


def parsear_guia(doc_txt, guia):
    if type(doc_txt) is str:
        doc_txt = io.StringIO(doc_txt)
    documento_formateado = prs_g.parser_guia(doc_txt)
    # print(guia)
    # print(documento_formateado)
    archivarJson.Archivar(documento_formateado, guia).archivar()

def get_json(doc_txt):
    if type(doc_txt) is str:
        doc_txt = io.StringIO(doc_txt)
    return json.dumps(prs_g.parser_guia(doc_txt), ensure_ascii=False)