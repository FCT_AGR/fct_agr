import re

from . import Identificadores as ident
from . import ParseadorEventosDimensiones as prs_e
from . import ParseardorComentarios as prs_c
from . import utils


def separar_pagename_propiedades(propiedades: str):
    propiedades = propiedades.split("\t")
    propiedades_devolver = dict()
    if "Sección" in propiedades:
        if propiedades[propiedades.index("Sección") + 1]:
            propiedades_devolver["Sección"] = propiedades[propiedades.index("Sección") + 1].strip()
        else:
            propiedades_devolver["Sección"] = ''
    if "Page type" in propiedades:
        if propiedades[propiedades.index("Page type") + 1]:
            propiedades_devolver["Page type"] = propiedades[propiedades.index("Page type") + 1].strip()
        else:
            propiedades_devolver["Page type"] = ''
    if "Page title" in propiedades:
        if propiedades[propiedades.index("Page title") + 1]:
            propiedades_devolver["Page title"] = propiedades[propiedades.index("Page title") + 1].strip()
        else:
            propiedades_devolver["Page title"] = ''

    return propiedades_devolver


def parsear_vistas(vista):
    vistas = []
    pos_previa = vista.tell()
    linea = utils.leer_lineas(vista)
    while ident.is_vista(linea):
        vista_temp = dict()
        # El nombre de la vista puede venir o no en nueva línea.
        if ident.is_vista_linea_nueva(linea):
            vista_temp["Vista"] = utils.leer_lineas(vista).strip()
        else:
            vista_temp["Vista"] = re.split(r'\t+', linea)[1]

        linea = utils.leer_lineas(vista)
        if ident.is_pagename(linea):
            vista_temp["Pagename"] = linea.split("\t")[1].strip()

            linea = utils.leer_lineas(vista)
            if ident.is_pagename_propiedades(linea):
                vista_temp.update(separar_pagename_propiedades(linea))
                vista_temp["Eventos y dimensiones"] = prs_e.parsear_eventos_dimensiones(vista)
                vista_temp["Comentarios"] = prs_c.parsear_comentario(vista)

        pos_previa = vista.tell()
        linea = utils.leer_lineas(vista)
        vistas.append(vista_temp)

    vista.seek(pos_previa)
    return vistas
