from . import ParseadorSubseccion as prs_ss

def parsear_error_subseccion(guia):
    err_seccion = dict()
    err_seccion["Índice"] = "****ERROR subseccion"
    err_seccion['Nombre'] =  "****ERROR subseccion"
    err_seccion["subseccion"] = [prs_ss.parsear_subseccion(guia)]
    return err_seccion