from . import ExpresionesRegulares as er


def is_seccion(linea: str) -> bool:
    return er.re_01_seccion.match(linea)


def is_subseccion(linea: str) -> bool:
    return er.re_02_subSeccion.match(linea)


def is_vista(linea: str) -> bool:
    return er.re_03_vista.match(linea)


def is_vista_linea_nueva(linea: str) -> bool:
    return er.re_03b_vista_en_linea_nueva.match(linea)


def is_pagename(linea: str) -> bool:
    return er.re_04_pagename.match(linea)


def is_pagename_propiedades(linea: str) -> bool:
    return er.re_05_seccion_pageType_pageTitle.match(linea)


def is_in_eventos(linea: str) -> bool:
    if ((not is_comentarios(linea)
         and not is_seccion(linea)
         and not is_subseccion(linea)
         and not is_vista(linea))
            or er.re_07b_saltar_ilustracion.match(linea)) and linea.strip() != '':
        return True
    else:
        return False


def is_evento(linea: str) -> bool:
    return er.re_06_eventos.match(linea)


def is_dimension(linea: str) -> bool:
    return er.re_07_dimensiones_pageview.match(linea)


def is_comentarios(linea: str) -> bool:
    return er.re_09_comentarios.match(linea)


def is_in_comentarios(linea: str) -> bool:
    if (not is_seccion(linea)
         and not is_subseccion(linea)
         and not is_vista(linea)):
        return True
    else:
        return False


def is_linea_blanca(linea: str) -> bool:
    return er.re_06b_lineas_blancas.match(linea)
