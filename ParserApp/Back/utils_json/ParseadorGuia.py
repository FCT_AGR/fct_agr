from . import Identificadores as ident
from . import ParseadorErrorSubseccion as prs_err_ss
from . import ParseadorErrorVista as prs_err_v
from . import ParseadorEventosDimensiones as prs_e
from . import ParseadorSeccion as prs_s
from . import utils


def parser_guia(guia):
    guia_parseada = []
    pos_previa = guia.tell()
    linea = utils.leer_lineas(guia)
    while linea != '':
        if ident.is_seccion(linea):
            guia.seek(pos_previa)
            guia_parseada.append(prs_s.parser_seccion(guia))
        # TODO En caso de error no siempre se monta bien la estructura
        else:
            if ident.is_subseccion(linea):
                guia.seek(pos_previa)
                guia_parseada.append(prs_err_ss.parsear_error_subseccion(guia))
            elif ident.is_vista(linea):
                guia.seek(pos_previa)
                guia_parseada.append(prs_err_v.parsear_error_vista(guia))
            elif ident.is_in_eventos(linea):
                guia.seek(pos_previa)
                error = dict()
                error["subseccion"] = "****ERROR eventos"
                error["subseccion"] = prs_e.parsear_eventos_dimensiones(guia)
                guia_parseada.append(error)
            else:
                error = dict()
                error["indice"] = "****ERROR inesperado"
                error["subseccion"] = linea
                guia_parseada.append(error)

        pos_previa = guia.tell()
        linea = utils.leer_lineas(guia)


    return guia_parseada