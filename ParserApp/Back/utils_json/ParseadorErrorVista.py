from . import ParseadorVistas as prs_v

def parsear_error_vista(guia):
    err_seccion = dict()
    err_seccion['Índice'] = "****ERROR vista"
    err_seccion['Nombre'] = "****ERROR vista"
    err_seccion["subseccion"] = prs_v.parsear_vistas(guia)

    return err_seccion
