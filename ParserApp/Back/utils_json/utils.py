import io
from . import Identificadores as ident

def leer_lineas(origen: io.TextIOWrapper) -> str:
    """
    Devuelve la siguiente línea que no esté en blanco
    Si se detecta que se ha llegado al final del documento se devuelve ''
    :param origen: Archivo a leer
    :return: Siguiente línea leída
    """
    pos = origen.tell()
    linea = origen.readline()
    #while linea == '':
    while ident.is_linea_blanca(linea):
        if pos == origen.tell():
            break
        pos = origen.tell()
        linea = origen.readline().strip()
    return linea