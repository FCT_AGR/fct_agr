from . import ParseadorSubseccion as prs_ss
from . import utils


def parser_seccion(guia):
    linea = utils.leer_lineas(guia)
    seccion = dict()
    seccion["Índice"] = linea.split("\t")[0].strip()
    seccion["Nombre"] = linea.split("\t")[1].strip()
    seccion["subseccion"] = prs_ss.parsear_subseccion(guia)
    return seccion