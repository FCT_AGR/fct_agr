import re

re_01_seccion = re.compile(r'^[\s\﻿ufeff]*\d+.\d+\s')

re_02_subSeccion = re.compile(r'^[\s]*\d+(.\d+)+\s')

# El nombre de la vista a veces viene en línea nueva
re_03_vista = re.compile(r'^[\s]*(VISTA|SECCIÓN|SECCION)')

# Aquí se distinque si el nombre de la línea viene o no en línea nueva
re_03b_vista_en_linea_nueva = re.compile(r'^[\s]*(VISTA|SECCIÓN|SECCION)\s?$')

re_04_pagename = re.compile(r'^[\s]*pagename\s', re.IGNORECASE)

re_05_seccion_pageType_pageTitle = re.compile(r'^[\s]*sección\s', re.IGNORECASE)

# El regex06 valida tanto eventos como las métricas personalizadas de pageview
re_06_eventos = re.compile(r'^[\s]*[\w|\s|\-|<|>|\/]*\t[\w|\s|\|\-|\<|\>|\/]+\t[\w|\s|\||\-|<|>|\/]+\t[\s|<|\w|>|:|\-|,>|\/]*[\w|\s|,]*\t[\w|\s|\.|\“|\”|\"|\(|\)>|\/]+')

# Con este regex se obvian la líneas de métricas de pageview en branco que se validan co re_05
re_06b_lineas_blancas= re.compile(r'^\s*$')

# Puede haber dos dimensiones en una línea
re_07_dimensiones_pageview = re.compile(r'^\s*(cd\d{1,3}|cm\d{1,3})\t[\w|\-| ]*\t')

re_07a_buscar_dimensiones = re.compile(r'cd\d{1,3}|cm\d{1,3}')

re_07b_saltar_ilustracion = re.compile(r'ilustraci', re.IGNORECASE)

re_09_comentarios = re.compile(r'^Comentarios\s*$')

re_09_EOF = re.compile('')
