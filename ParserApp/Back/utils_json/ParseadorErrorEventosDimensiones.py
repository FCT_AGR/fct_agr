from . import ParseadorEventosDimensiones as prs_e


def parsear_error_eventos_dimensiones(vista):
    err_vista = dict()
    err_vista["Vista"] = "ERROR eventos/dimensiones"
    err_vista['Pagename'] = "ERROR eventos/dimensiones"
    err_vista["Eventos y dimensiones"] = prs_e.parsear_eventos_dimensiones(vista)

    err_subseccion = dict()
    err_subseccion['Índice'] = "ERROR eventos/dimensiones"
    err_subseccion['Nombre'] = "ERROR eventos/dimensiones"
    err_subseccion['Vistas'] = [err_vista]

    err_seccion = dict()
    err_seccion['Índice'] = "ERROR eventos/dimensiones"
    err_seccion["subseccion"] = [err_subseccion]