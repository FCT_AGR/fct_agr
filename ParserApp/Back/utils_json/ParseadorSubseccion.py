from . import Identificadores as ident
from . import ParseadorVistas as prs_v
from . import utils


def parsear_subseccion(seccion):
    subseccion = []
    pos_previa = seccion.tell()
    linea = utils.leer_lineas(seccion)

    if ident.is_vista(linea):
        subseccion_temp = dict()
        subseccion_temp["Índice"] = ''
        subseccion_temp["Nombre"] = ''
        seccion.seek(pos_previa)
        subseccion_temp["Vistas"] = prs_v.parsear_vistas(seccion)

        subseccion.append(subseccion_temp)
        pos_previa = seccion.tell()
        linea = utils.leer_lineas(seccion)

    while ident.is_subseccion(linea):
        subseccion_temp = dict()
        subseccion_temp["Índice"] = linea.split("\t")[0].strip()
        subseccion_temp["Nombre"] = linea.split("\t")[1].strip()

        # Si hay una sub-subsección dentro de la subsección:
        pos_previa = seccion.tell()
        linea = utils.leer_lineas(seccion)
        if ident.is_subseccion(linea):
            continue
        else:
            seccion.seek(pos_previa)

        subseccion_temp["Vistas"] = prs_v.parsear_vistas(seccion)

        subseccion.append(subseccion_temp)
        pos_previa = seccion.tell()
        linea = utils.leer_lineas(seccion)

    seccion.seek(pos_previa)
    return subseccion