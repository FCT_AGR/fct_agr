from . import Identificadores as ident
from . import utils

def parsear_comentario(comentarios):
    comentarios_json = ""
    pos_previa = comentarios.tell()
    linea = utils.leer_lineas(comentarios)

    while ident.is_in_comentarios(linea):
        if linea == '':
            break
        elif not ident.is_comentarios(linea):
            # avoid if linea is 'Comentarios'
            comentarios_json += linea

        pos_previa = comentarios.tell()
        linea = utils.leer_lineas(comentarios)

    comentarios.seek(pos_previa)
    return comentarios_json
