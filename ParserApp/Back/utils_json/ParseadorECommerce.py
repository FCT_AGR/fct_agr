from . import ExpresionesRegulares as er
from . import Identificadores as ident
from . import utils

'''
datos_ecommerce:
[
    [
        "nombre_evento": "",
        "tipo_datos": "",
        "lista": ""
    ],
]
'''
def parsearECommerce(seccion_ecommerce):
    '''
    Se identifica la cabecera de la tabla de eCommerce.
    :return:
    '''
    linea = utils.leer_lineas(seccion_ecommerce)
    datos_ecommerce = []
    lista = ''

    while ident.is_in_ecommerce(linea):
        obj_ecommerce = dict()
        nombre_evento = ''
        tipo_datos = ''

        if ident.is_ecommerce_table_header(linea):
            # En caso de encontrar un comienzo de tabla se reinicia el valor de lista
            lista = ''
            continue
        elif ident.is_ecommerce_all_in_line(linea):
            datos = linea.split("\t")
            nombre_evento = datos[0].strip()
            tipo_datos = datos[1].strip()
            lista = datos[3].strip()
        elif ident.is_ecommerce_list_in_new_line(linea):
            # Comienza con un tabulador
            pass
        elif ident.is_ecommerce_pageview_or_evento(linea):
            # \w\t\w
            pass
        else:
            continue

        obj_ecommerce["nombre_evento"] = nombre_evento
        obj_ecommerce["tipo_datos"] = tipo_datos
        obj_ecommerce["lista"] = lista
        datos_ecommerce.append(obj_ecommerce)

    return datos_ecommerce