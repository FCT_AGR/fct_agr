import re

from . import ExpresionesRegulares as er
from . import Identificadores as ident
from . import utils


def procesar_eventos_en_dos_lineas(primera_linea: str, segunda_linea: str) -> str:
    """
    En el medio de la descripción de un evento a veces aparece un salto de línea.
    Aquí se trata de detectar y recomponer el evento
    :param primera_linea: Primera parte del posible evento
    :param segunda_linea: Segunta parte del evento
    :return: Se devuelven los datos listos para ser incorporados a la lista de eventos
    """

    linea = primera_linea[:-1] + "\t" + segunda_linea
    if linea.count('\t') == 5 and ident.is_evento(linea):
            return separar_eventos(linea)
    else:
        return ""


def separar_dimensiones_pageview(dimensions: str):
    return re.findall(er.re_07a_buscar_dimensiones, dimensions)


def separar_eventos(eventos: str):
    lista_eventos = dict()
    eventos = eventos.split("\t")
    lista_eventos["C.F."] = eventos[0].strip()
    lista_eventos["Categoría"] = eventos[1].strip()
    lista_eventos["Acción"] = eventos[2].strip()
    lista_eventos["Etiqueta"] = eventos[3].strip()
    # this avoid store blank dimensions
    dimension = [x.strip() for x in eventos[4].split(',')]
    if len(dimension) != 1 and dimension[0] != '':
        lista_eventos["Dimens."] = dimension
    if len(eventos) > 5:
        lista_eventos["Lanzamiento"] = eventos[5].strip()
    else:
        lista_eventos["Lanzamiento"] = ''

    return lista_eventos


def parsear_eventos_dimensiones(lista_eventos_dimensiones):
    eventos = []
    dimensiones = []
    evento_en_dos_lineas = ""
    pos_previa = lista_eventos_dimensiones.tell()
    linea = utils.leer_lineas(lista_eventos_dimensiones)

    while ident.is_in_eventos(linea):
        if ident.is_linea_blanca(linea):
            pass
        elif ident.is_dimension(linea):
            dimensiones.extend(separar_dimensiones_pageview(linea))
            evento_en_dos_lineas = ""
        elif ident.is_evento(linea):
            eventos.append(separar_eventos(linea))
            evento_en_dos_lineas = ""
        else:
            if evento_en_dos_lineas == "":
                evento_en_dos_lineas = linea
            else:
                evento_procesado = procesar_eventos_en_dos_lineas(evento_en_dos_lineas, linea)
                if evento_procesado:
                    eventos.append(evento_procesado)
                    evento_en_dos_lineas = ""
                else:
                    evento_en_dos_lineas = linea
        pos_previa = lista_eventos_dimensiones.tell()
        linea = utils.leer_lineas(lista_eventos_dimensiones)

    lista_eventos_dimensiones.seek(pos_previa)
    return [eventos, dimensiones]