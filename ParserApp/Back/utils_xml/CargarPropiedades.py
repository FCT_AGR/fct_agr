import xml.etree.cElementTree as ET

from ParserApp import models
from ParserApp.Back.utils_xml import TraducirGuiaToRegexp as ToRegexp


class CargadorPropiedades:
    def __init__(self, guia, dispositivo, diccionarios):
        self.guia = guia
        self.dispositivo = dispositivo
        self.dict_dimensiones = diccionarios['dimensiones']
        self.dict_variables = diccionarios['variables']
        self.seccion_error = 'ToXml'
        self.hit = None
        self.et_padre = None

    def buscar_regex(self, dimension: str):
        if dimension in self.dict_dimensiones:
            return self.dict_dimensiones[dimension]['regexp']
        else:
            print('Dimensión desconocida: \"' + dimension + '\"')
            models.ErrorGuia.objects.create(
                guia=self.guia,
                seccion=self.seccion_error,
                error='Dimensión desconocida: \"' + dimension + '\"'
            )
            return ".*"

    def cargar_propiedad(self, dimension, valor_a_traducir='', traduccion_literal='', peso='10.0', pageview=""):
        if pageview != '':
            hit = pageview
        else:
            hit = self.hit

        if traduccion_literal != '':
            regexp = traduccion_literal
        elif valor_a_traducir != '' and valor_a_traducir in hit and hit[valor_a_traducir] != '':
            regexp = ToRegexp.traducir(hit[valor_a_traducir], self.dict_variables)
        else:
            regexp = self.buscar_regex(dimension)
            # ------------------------------------------------------
            # PROBANDO VALORES PARA LA CORRECTA ASIGNACIÓN DE PLANTILLA
            # ------------------------------------------------------
            peso = '1.0' # Si se carga una expresión genérica se baja el peso

        if regexp == '.*' or regexp == '.+':
            peso = '1.0'

        xml_hv_properties = ET.SubElement(self.et_padre, 'properties')
        ET.SubElement(xml_hv_properties, 'name').text = dimension
        ET.SubElement(xml_hv_properties, 'reg-exp').text = '(?i)' + regexp
        ET.SubElement(xml_hv_properties, 'eq-pat').text = 'eq'
        ET.SubElement(xml_hv_properties, 'eq-weight').text = peso

    def propiedades_evento(self, et_padre, evento, pageview, cargar_cd68=True):
        self.et_padre = et_padre
        self.hit = evento

        if str(self.dispositivo) == 'app':
            self.cargar_propiedad('an', pageview=pageview)
            self.cargar_propiedad('av', pageview=pageview)
            self.cargar_propiedad('cd', pageview=pageview, valor_a_traducir='Pagename', peso='90.0')
        else:
            self.cargar_propiedad('dp', pageview=pageview, valor_a_traducir='Pagename', peso='90.0')
            self.cargar_propiedad('gtm', peso='100.0')

        # Se evita enviar label = '.*'
        if evento['Etiqueta'] != "":
            self.cargar_propiedad('el', valor_a_traducir='Etiqueta', peso='20.0')

        self.cargar_propiedad('ec', valor_a_traducir='Categoría', peso='100.0')
        self.cargar_propiedad('ea', valor_a_traducir='Acción', peso='100.0')
        self.cargar_propiedad('t', traduccion_literal='event', peso='100.0')
        self.cargar_propiedad('cd1')
        self.cargar_propiedad('cd2')
        self.cargar_propiedad('cd4')
        self.cargar_propiedad('cd11')
        self.cargar_propiedad('cd12')
        self.cargar_propiedad('cd63')
        self.cargar_propiedad('cd67', pageview=pageview, valor_a_traducir='Sección', peso='60.0')
        if cargar_cd68:
            self.cargar_propiedad('cd68')
        self.cargar_propiedad('cd73')
        self.cargar_propiedad('cd77', valor_a_traducir='C.F.', peso='100.0')
        # TODO Es preferible que se establezca un peso alto si la expresión regular es específica y bajo si es genérica
        self.cargar_propiedad('cd90', pageview=pageview, valor_a_traducir='Page type', peso='60.0')
        self.cargar_propiedad('cd92')
        self.cargar_propiedad('tid', peso='100.0')
        for propiedad in evento['Dimens.']:
            if propiedad != '': self.cargar_propiedad(propiedad)

    def propiedades_pageview(self, et_padre, pageview, cargar_cd68=True):
        self.et_padre = et_padre
        self.hit = pageview

        # TODO Es preferible que se establezca un peso alto si la expresión regular es específica y bajo si es genérica
        if str(self.dispositivo) == 'app':
            self.cargar_propiedad('an')
            self.cargar_propiedad('av')
            self.cargar_propiedad('cd', valor_a_traducir='Pagename', peso='90.0')
            self.cargar_propiedad('t', traduccion_literal='(appview|screenview)', peso='100.0')
        else:
            self.cargar_propiedad('dp', valor_a_traducir='Pagename', peso='90.0')
            self.cargar_propiedad('gtm', peso='100.0')
            self.cargar_propiedad('t', traduccion_literal='pageview', peso='100.0')

        self.cargar_propiedad('cd1')
        self.cargar_propiedad('cd11')
        self.cargar_propiedad('cd12')
        self.cargar_propiedad('cd2')
        self.cargar_propiedad('cd4')
        self.cargar_propiedad('cd63')
        self.cargar_propiedad('cd67', valor_a_traducir='Sección', peso='60.0')
        if cargar_cd68:
            self.cargar_propiedad('cd68')
        self.cargar_propiedad('cd73')
        self.cargar_propiedad('cd90', valor_a_traducir='Page type', peso='60.0')
        self.cargar_propiedad('cd92')
        self.cargar_propiedad('tid', peso='100.0')
        try:
            for propiedad in pageview['Eventos y dimensiones'][1]:
                if propiedad != '':
                    self.cargar_propiedad(propiedad)
        except:
            pass;
