from ParserApp import models


def get_subseccion(seccion):
    subsecciones_serializados = []
    for subseccion in models.Subseccion.objects.filter(seccion=seccion.pk):
        if models.Subseccion.objects.filter(subseccion=subseccion.pk):
            get_subseccion(subseccion)
        subseccion_serializada = dict()
        vistas_serializadas = []
        for vista in models.Vista.objects.filter(subseccion=subseccion.pk):
            eventos_dimensiones_serializados = []
            eventos_serializados = []
            for evento in models.Evento.objects.filter(vista=vista.pk):
                dimensiones_serializada = []
                for dimension in models.DimensionesEvento.objects.filter(evento=evento.pk):
                    dimensiones_serializada.append(dimension.dimension)
                evento_serializado = {
                    'Acción': evento.accion,
                    'C.F.': evento.clase_funcional,
                    'Categoría': evento.categoria,
                    'Dimens.': dimensiones_serializada,
                    'Etiqueta': evento.etiqueta,
                    'Lanzamiento': evento.lanzamiento
                }
                eventos_serializados.append(evento_serializado)

            dimensiones_pageview = []
            for dimension in models.DimensionesPageview.objects.filter(vista=vista.pk):
                dimensiones_pageview.append(dimension.dimension)

            eventos_dimensiones_serializados.append(eventos_serializados)
            eventos_dimensiones_serializados.append(dimensiones_pageview)

            vista_serializada = dict()
            vista_serializada["Vista"] = vista.vista
            vista_serializada["Pagename"] = vista.pagename
            vista_serializada["Page title"] = vista.pagetitle
            vista_serializada['Page type'] = vista.pagetype
            vista_serializada["Sección"] = vista.seccion
            vista_serializada['Eventos y dimensiones'] = eventos_dimensiones_serializados

            vistas_serializadas.append(vista_serializada)

        subseccion_serializada['Nombre'] = subseccion.nombre
        subseccion_serializada['Índice'] = subseccion.indice
        subseccion_serializada['Vistas'] = vistas_serializadas

        subsecciones_serializados.append(subseccion_serializada)

    seccion_serializado = {
        'Nombre': seccion.nombre,
        'Índice': seccion.indice,
        'subseccion': subsecciones_serializados
    }

    return seccion_serializado

def getJson(guia):
    json_serializado = []
    for seccion in models.Seccion.objects.filter(json=guia.json.pk):
        seccion_serializado = get_subseccion(seccion)

        json_serializado.append(seccion_serializado)

    return json_serializado