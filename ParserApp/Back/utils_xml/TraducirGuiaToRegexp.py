def traducir(linea, diccionario):
    if linea:
        for variable in diccionario:
            linea = linea.replace(variable, diccionario[variable])
    return linea
