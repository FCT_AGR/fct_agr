from ParserApp import models



"""
Estructura dicc_dimensiones:
    'parámetro': {
        'regexp': r'expresión regular',
        'dl': 'referencia dataLayer',
        'ejemp': 'valor ejemplo',
        'nombre': 'nombre',
        'desc': 'descripción'
    }
"""

def get_dicc_variables(nombre_guia, plataforma):
    diccionario = dict()
    try:
        cadena_obj = models.Cadena.objects.filter(nombre=nombre_guia)[0]
        plataforma_obj = models.Plataforma.objects.filter(nombre=plataforma)[0]
        diccionario_obj = models.DiccionarioVariable.objects.filter(cadena=cadena_obj, plataforma=plataforma_obj)
        if len(diccionario_obj) > 0:
            variables = models.Variable.objects.filter(diccionario=diccionario_obj[0]).all()
            for variable in variables.iterator():
                diccionario[variable.variable] = variable.regexp.regexp
    except IndexError as e:
        print('¿No se ha definido un diccionario de variables básico?:')
        print(str(e))

    return  diccionario


def get_diccionario_variables(nombre_guia, plataforma):
    diccionario_variables = {**get_dicc_variables('unificacion', 'unificacion'),
                             **get_dicc_variables('unificacion', plataforma),
                             **get_dicc_variables(nombre_guia, 'unificacion'),
                             **get_dicc_variables(nombre_guia, plataforma)}
    return diccionario_variables


def get_dicc_dimens(nombre_guia, plataforma):
    diccionario = dict()

    try:
        cadena_obj = models.Cadena.objects.filter(nombre=nombre_guia)[0]
        plataforma_obj = models.Plataforma.objects.filter(nombre=plataforma)[0]
        diccionario_obj = models.DiccionarioDimensiones.objects.filter(cadena=cadena_obj, plataforma=plataforma_obj)
        dimensiones = models.Dimension.objects.filter(diccionario=diccionario_obj[0]).all()
        for dimension in dimensiones.iterator():
            datos_dimension = {'regexp': dimension.regexp.regexp,
                               'dl': dimension.datalayer,
                               'ejemp': dimension.ejemplo,
                               'nombre': dimension.nombre,
                               'desc': dimension.descripcion}
            diccionario[dimension.parametro] = datos_dimension
    except IndexError as e:
        pass
        # print('¿No se ha definido un diccionario de dimensiones básico?:')
        # print(str(e))

    return diccionario


def get_diccionario_dimensiones(nombre_guia, plataforma):
    diccionario_dimens = {**get_dicc_dimens('unificacion', 'unificacion'),
                          **get_dicc_dimens('unificacion', plataforma),
                          **get_dicc_dimens(nombre_guia, 'unificacion'),
                          **get_dicc_dimens(nombre_guia, plataforma),}
    return diccionario_dimens


def get_diccionarios(nombre_guia, plataforma):
    diccionarios = {'dimensiones': get_diccionario_dimensiones(nombre_guia, plataforma),
                    'variables': get_diccionario_variables(nombre_guia, plataforma)}

    return diccionarios
