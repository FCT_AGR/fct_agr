from django.shortcuts import render, HttpResponseRedirect
from django.views.generic import ListView, DetailView, UpdateView, CreateView
from django.http import HttpResponse
from .forms import GuiaForm
from .Back.Test import make_all_tests
from ParserApp.Back import JsonToXml as ToXml, JsonToCsv as ToCsv
from ParserApp.models import Guia
import codecs

from . import models


# Create your views here.
def home_page(request):
    return render(request, 'parserapp/home_parser_app.html')


class GuiasList(ListView):
    model = models.Guia

    ordering = ['cadena', 'plataforma', 'version']


class GuiasDetail(DetailView):
    model = models.Guia


class GuiaUpdateView(UpdateView):
    redirect_field_name = 'parserapp/guia_detail.html'

    form_class = GuiaForm
    model = models.Guia


class CreateGuiaView(CreateView):
    redirect_field_name = 'parserapp/guia_detail.html'

    form_class = GuiaForm
    model = models.Guia


class DiccionariosDimensionesList(ListView):
    model = models.DiccionarioDimensiones


class DiccionarioDimensionesDetail(DetailView):
    model = models.DiccionarioDimensiones


class DiccionariosVariableList(ListView):
    model = models.DiccionarioVariable


class DiccionarioVariableDetail(DetailView):
    model = models.DiccionarioVariable


class ExpresionRegular(ListView):
    model = models.ExpresionRegular


def guardar_xml(request, pk):
    guia = Guia.objects.get(pk=pk)
    archivar_xml = ToXml.ConvertirAXml(guia)
    salida = archivar_xml.convertir_a_xml(False)
    filename = str(guia) + '.xml'

    response = HttpResponse(content_type='text/xml')
    response['Content-Disposition'] = 'attachment; filename="' + filename + '"'
    response.write(salida)
    return response


def guardar_csv(request, pk):
    guia = Guia.objects.get(pk=pk)
    archivar_csv = ToCsv.ArchivarCsv(guia)
    csv_txt = archivar_csv.convertir_a_csv(False)
    salida = str(csv_txt, 'utf-8-sig')
    filename = str(guia) + '.csv'

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="' + filename + '"'

    response.write(salida)
    return response

def test(request):
    make_all_tests()

    return render(request, 'base.html')