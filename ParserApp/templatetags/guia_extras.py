from django import template

register = template.Library()


@register.filter
def errores_en_seccion(errores, seccion):
    return errores.filter(seccion=seccion)


register.filter('errores_en_seccion', errores_en_seccion)
