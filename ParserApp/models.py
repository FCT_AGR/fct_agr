import datetime
from django import forms
from django.db import models
from django.urls import reverse
from django.contrib import admin

from ParserApp.Back import JsonToXml as ToXml, ToJson, JsonToCsv as ToCsv


# Poryecto, Cadena, plataforma --------------------------------------
class Proyecto(models.Model):
    nombre = models.CharField(max_length=128)
    fields = ('nombre',)

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre


# 'unificacion' es el valor para usar en los diccionarios como base
class Cadena(models.Model):
    nombre = models.CharField(max_length=128)
    proyecto = models.ForeignKey('Proyecto',
                                 on_delete=models.PROTECT,
                                 related_name='proyecto')

    class Meta:
        ordering = ['proyecto', 'nombre']

    def __str__(self):
        return str(self.proyecto) + " " + self.nombre


# app | web
class Plataforma(models.Model):
    nombre = models.CharField(max_length=64, primary_key=True)

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        return self.nombre


# Expresiones Regulares -------------------------
class ExpresionRegular(models.Model):
    proyecto = models.ForeignKey('Proyecto', on_delete=models.PROTECT, related_name='expresionregular')
    nombre = models.CharField(max_length=256, primary_key=True)
    regexp = models.CharField(max_length=1024, default='.+')

    class Meta:
        ordering = ['nombre']

    def __str__(self):
        if len(str(self.regexp)) > 50:
            mostrar_regexp = self.regexp[0:50] + '…'
        else:
            mostrar_regexp = self.regexp
        return self.nombre


# Diccionario de variables de las guías -------------------------
class DiccionarioVariable(models.Model):
    cadena = models.ForeignKey('Cadena',
                               on_delete=models.PROTECT,
                               related_name='diccionario_variable')
    plataforma = models.ForeignKey('Plataforma',
                                   on_delete=models.PROTECT,
                                   related_name='diccionario_variable')

    class Meta:
        ordering = ['cadena', 'plataforma']
        unique_together = (('cadena', 'plataforma'),)

    def __str__(self):
        return str(self.cadena) + " - " + str(self.plataforma)


class Variable(models.Model):
    variable = models.CharField(max_length=128)
    diccionario = models.ForeignKey('DiccionarioVariable',
                                    on_delete=models.CASCADE,
                                    related_name='variable')
    regexp = models.ForeignKey('ExpresionRegular',
                                         on_delete=models.CASCADE,
                                         related_name='variable_guia')

    def __str__(self):
        return str(self.diccionario) + ' | ' + str(self.variable)

    class Meta:
        ordering = ['diccionario', 'variable',]
        unique_together = (('variable', 'diccionario'),)


# Diccionario dimensiones ---------------------------------
class DiccionarioDimensiones(models.Model):
    cadena = models.ForeignKey('Cadena', on_delete=models.PROTECT,
                               related_name='diccionario_dimension')
    plataforma = models.ForeignKey('Plataforma', on_delete=models.PROTECT,
                                   related_name='diccionario_dimension')

    class Meta:
        ordering = ['cadena', 'plataforma']
        unique_together = (('cadena', 'plataforma'),)

    def __str__(self):
        return str(self.cadena) + " - " + str(self.plataforma)


class Dimension(models.Model):
    parametro = models.CharField(max_length=32)
    diccionario = models.ForeignKey('DiccionarioDimensiones', on_delete=models.CASCADE,
                                    related_name='dimension')
    regexp = models.ForeignKey('ExpresionRegular',
                                         on_delete=models.CASCADE,
                                         related_name='dimension')
    nombre = models.CharField(max_length=256, blank=True)
    datalayer = models.CharField(max_length=512, blank=True)
    ejemplo = models.CharField(max_length=512, blank=True)
    descripcion = models.TextField(blank=True)

    def __str__(self):
        return str(self.diccionario) + ' | ' + self.parametro + " (" + self.nombre + ")"

    class Meta:
        ordering = ['diccionario', 'parametro',]
        unique_together = (('parametro', 'diccionario'),)


# Guía ------------------------------------------------------
class Guia(models.Model):
    # proyecto = models.ForeignKey('Proyecto', on_delete=models.PROTECT, related_name='guia')
    cadena = models.ForeignKey('Cadena', on_delete=models.PROTECT, related_name='guias')
    plataforma = models.ForeignKey('Plataforma', on_delete=models.PROTECT, related_name='guias')
    version = models.CharField(max_length=32)
    txt = models.TextField(blank=True)

    def __str__(self):
        return str(self.cadena) + " - " + str(self.plataforma) + " - " + str(self.version)

    def get_absolute_url(self):
        return reverse("parserapp:guia_detail", args=[self.pk])

    def save(self, *args, **kwargs):
        for error in ErrorGuia.objects.filter(guia=self.id):
            error.delete()
        if Guia.objects.filter(id=self.id).count() > 0:
            self.delete()

        super(Guia, self).save(*args, **kwargs)

        # print('******************* ToJson *******************')
        ToJson.parsear_guia(doc_txt=self.txt, guia=self)
        print('******************* ToXml *******************')
        archivar_xml = ToXml.ConvertirAXml(guia=self)
        archivar_xml.convertir_a_xml(True)
        print('******************* ToCsv *******************')
        archivar_csv = ToCsv.ArchivarCsv(guia=self)
        archivar_csv.convertir_a_csv(True)

    class Meta:
        ordering = ['cadena', 'plataforma']


class ErrorGuia(models.Model):
    guia = models.ForeignKey('Guia', on_delete=models.CASCADE, related_name='error')
    seccion = models.CharField(max_length=64)
    error = models.TextField(blank=True)

    def __str__(self):
        return str(self.guia) + ' | ' + str(self.seccion)


# Json ------------------------------------------------------
class Json(models.Model):
    guia = models.OneToOneField(Guia, on_delete=models.CASCADE, primary_key=True)
    fecha_creacion = models.DateField(default=datetime.date.today)
    txt = models.TextField(blank=True, null=True)

    def __str__(self):
        return str(self.guia)

    def get_absolute_url(self):
        return reverse("parserapp:guias")


class Seccion(models.Model):
    json = models.ForeignKey('Json', on_delete=models.CASCADE, related_name='secciones')
    nombre = models.CharField(max_length=128, blank=True)
    indice = models.CharField(max_length=32)

    def __str__(self):
        return str(self.indice) + "- " + str(self.nombre)


class Subseccion(models.Model):
    seccion = models.ForeignKey('Seccion', on_delete=models.CASCADE, related_name='subsecciones', blank=True, null=True)
    subseccion_padre = models.ForeignKey("self", models.CASCADE, blank=True, null=True)
    nombre = models.CharField(max_length=128, blank=True)
    indice = models.CharField(max_length=32)

    def __str__(self):
        return str(self.indice) + "- " + str(self.nombre)


class Vista(models.Model):
    subseccion = models.ForeignKey('Subseccion', models.CASCADE, related_name='vistas')
    vista = models.CharField(max_length=128)
    pagename = models.CharField(max_length=256, blank=True)
    pagetitle = models.CharField(max_length=256, blank=True)
    pagetype = models.CharField(max_length=128, blank=True)
    seccion = models.CharField(max_length=128, blank=True)
    comentarios = models.TextField(blank=True)

    def __str__(self):
        return self.vista


class DimensionesPageview(models.Model):
    vista = models.ForeignKey('Vista', models.CASCADE, related_name='dimensionespageview')
    dimension = models.CharField(max_length=32)

    def dimension_datos(self):
        dimension_obj = dict()
        try:
            dimension_obj = Dimension.objects.get(parametro=self.dimension)
        except (Dimension.DoesNotExist, Dimension.MultipleObjectsReturned):
            return ['', '']

        return [dimension_obj.nombre, dimension_obj.ejemplo]


class Evento(models.Model):
    vista = models.ForeignKey('Vista', models.CASCADE, related_name='eventos')
    accion = models.CharField(max_length=128)
    clase_funcional = models.CharField(max_length=128)
    categoria = models.CharField(max_length=128)
    etiqueta = models.CharField(max_length=128, blank=True)
    lanzamiento = models.CharField(max_length=256, blank=True)

    def __str__(self):
        return self.accion


class DimensionesEvento(models.Model):
    evento = models.ForeignKey('Evento', models.CASCADE, related_name='dimensionesevento')
    dimension = models.CharField(max_length=32)

    def __str__(self):
        return str(self.evento) + ' | ' + str(self.dimension)


# Test ------------------------------------------------------
class Test(models.Model):
    guia = models.ForeignKey(Guia, on_delete=models.CASCADE, related_name='test')
    json = models.TextField()
    csv = models.TextField()
    xml = models.TextField()

    def __str__(self):
        return str(self.guia)
